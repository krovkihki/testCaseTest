<?php

namespace Database\Seeders;

use Database\Factories\PeopleFactory;
use Illuminate\Database\Seeder;
use App\Models\People;

class PeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       People::factory()->count(100)->create();
    }
}
