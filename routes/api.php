<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    "namespace" => "\App\Http\Controllers\Api\\",
], function () {
    Route::group(['prefix' => '/people'], function () {
        Route::get('', 'PeopleController@index');
        Route::get('/{id}', 'PeopleController@show');
        Route::post('/', 'PeopleController@store');
        Route::post('/{id}', 'PeopleController@update');
        Route::delete('/{id}', 'PeopleController@destroy');
    });
});
