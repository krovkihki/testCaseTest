<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Responders\CreatedResponder;
use App\Responders\NoContentResponder;
use App\Services\PeopleService;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(PeopleService $peopleService)
    {
        $this->peopleService = $peopleService;
    }

    public function index()
    {
        return $this->peopleService->list();
    }

    public function show(int $id)
    {
        return $this->peopleService->show($id);
    }

    public function store(Request $request)
    {
        $this->peopleService->store($request);
        return CreatedResponder::response();
    }


    public function update(Request $request, $id)
    {
        $this->peopleService->update($request,$id);
        return NoContentResponder::response();
    }

    public function destroy(int $id)
    {
        $this->peopleService->destroy($id);
        return NoContentResponder::response();
    }
}
