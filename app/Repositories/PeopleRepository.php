<?php

namespace App\Repositories;

use App\Models\People;

class PeopleRepository
{

    public static function list()
    {

        return People::all();

    }

    public static function show(int $id)
    {
        return People::where('id', $id)->firstOrFail();
    }
}
