<?php

namespace App\Responders;

class NoContentResponder
{
    public static function response()
    {
        return response('', 204);
    }
}
