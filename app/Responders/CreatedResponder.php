<?php

namespace App\Responders;

class CreatedResponder
{
    public static function response()
    {
        return response('', 201);
    }
}
