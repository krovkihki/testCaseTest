<?php

namespace App\Services;

use App\Models\People;
use App\Repositories\PeopleRepository;
use Illuminate\Http\Request;

class PeopleService
{

    public function list()
    {
        return PeopleRepository::list();
    }

    public function show(int $id)
    {
        return PeopleRepository::show($id);
    }

    public function store(Request $request)
    {
        $model = new People();
        $data = $request->only(
            'name',
            'age',
        );
        $model->setRawAttributes($data)->save();
    }

    public function update(Request $request, $id)
    {
        $model = $this->show($id);
        $data = $request->only(
            'name',
            'age'
        );
        $model->setRawAttributes($data)->save();
    }

    public function destroy(int $id)
    {
         return PeopleRepository::show($id)->delete();

    }
}

