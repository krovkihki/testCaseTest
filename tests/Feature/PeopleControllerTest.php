<?php

namespace Tests\Feature;

use App\Models\People;
use App\Repositories\PeopleRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class PeopleControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function setUp() :void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed --class PeopleSeeder');
    }

    public function test_index()
    {
        $response = $this->get('api/people');
        $response->assertOk();
    }

    public function test_show()
    {
        $id_rand = DB::table('people')->select('id')->max('id');
        $response = $this->get('api/people/' . $id_rand);
        $response->assertOk();
    }

    public function test_store()
    {
        $response = $this->postJson('/api/people/', ['name' => 'Sally', 'age' => '18']);
        $response->assertStatus(201);
    }

    public function test_update()
    {
        $id_rand = DB::table('people')->select('id')->max('id');
        $response = $this->postJson('/api/people/'.$id_rand, ['name' => 'Sally', 'age' => '18']);
        $response->assertStatus(204);
    }

    public function test_delete()
    {
        $id_rand = DB::table('people')->select('id')->max('id');
        $response = $this->delete('/api/people/'.$id_rand);
        $response->assertStatus(204);
    }

}
